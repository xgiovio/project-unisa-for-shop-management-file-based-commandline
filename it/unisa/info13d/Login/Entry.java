package it.unisa.info13d.Login;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 17/12/13
 * Time: 0.38
 */
/**
 * 
 * Classe che rappresenta un utente all'interno del "database" su disco.
 *
 */
public class Entry implements Serializable{

	/**
	 * Il costruttore della classe si occupa di inizializzare i dati dell'utente
	 * @param in_user - username utente
	 * @param in_password - password utente
	 * @param in_type - tipologia di utente
	 * @param in_location - luogo abitazione utente
	 */
	public Entry (String in_user, String in_password, boolean in_type, String in_location) {
        user = in_user;
        password = in_password;
        type = in_type;
        location = in_location;
        storico = new ArrayList<StoricoItem>();
    }
	
    /**
     * 
     * @return - username of user
     */
    public String getUser() {
        return user;
    }

    /**
     * 
     * @return - password of user
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @return - il tipo di utente
     */
    public boolean getType() {
        return type;
    }

    /**
     * 
     * @return - il luogo di abitazione dell'utente
     */
    public String getLocation() {
        return location;
    }

    /**
     * 
     * @return - il saldo dell'utente
     */
    public double getBalance() {
        return balance;
    }

    /**
     * 
     * Questo metodo aggiugne credito al saldo dell'utente.
     * @param balance - valore della ricarica del saldo
     */
    public void setBalance(double balance) {
        this.balance += balance;
 
    }
    /**
     * 
     * @return - un ArrayList che rappresenta lo storico degli acquisti dell'utente
     */
    public ArrayList<StoricoItem> getStorico() {
        return storico;
    }

    private String user;
    private String password;
    private String location;
    private double balance;
    private boolean type;
    private ArrayList<StoricoItem> storico;

}
