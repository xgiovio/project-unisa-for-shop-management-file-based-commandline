package it.unisa.info13d.Login;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 17/12/13
 * Time: 18.27
 */

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Questa classe rappresenta un singolo acquisto fatto dall'utente.
 */
public class StoricoItem implements Serializable {
    
	/**
     * 
     * @param in_description - descrizione articolo acquistato
     * @param in_data_acquisto - data dell'acquisto (Generata al momento dell'acquisto)
     * @param in_prezzo - prezzo dell'articolo
     */
    public StoricoItem (String in_description, GregorianCalendar in_data_acquisto, double in_prezzo){
        description = in_description;
        data_acquisto = in_data_acquisto;
        prezzo = in_prezzo;

    }

	/**
	 * 
	 * @return - descrizione deall'articolo acquistato
	 */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @return - la data dell'acquisto dell'oggeto
     */
    public GregorianCalendar getData_acquisto() {
        return data_acquisto;
    }
    /**
     * 
     * @return - il prezzo dell'oggetto acquistato
     */
    public double getPrezzo() {
        return prezzo;
    }

    private String description;
    private GregorianCalendar data_acquisto;
    private double prezzo;

}
