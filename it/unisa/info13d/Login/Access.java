package it.unisa.info13d.Login;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 19.45
 */
public class Access {
    /**
     *  Questo metodo avvia la procedura di login o di registrazione dell'utente in base alla scelta fatta nel menu.
     *
     * @param set_user_logged_here dove memorizzare il nome dell'utente loggato o registrato
     * @return false se Amministratore, true se Utente
     */
    public static boolean get_access (LoggedUser set_user_logged_here) throws  FileNotFoundException,IOException,ClassNotFoundException{

        System.out.println("---------- Benvenuto ----------");
        Scanner reader = new Scanner(System.in);


        System.out.println("1 --> Login");
        System.out.println("2 --> Registrazione");

        String choice;
        System.out.print("\nScelta operazione: ");
        choice = reader.nextLine();
        for ( ;  !(choice.equals("1")) && !(choice.equals("2"))  ;){
            System.out.println("Scelta Errata. Riprovare");
            System.out.print("\nScelta operazione: ");
            choice = reader.nextLine();
        }

        if  (choice.equals("1")){
        	System.out.println("---------- Login ----------");
            return login(set_user_logged_here);
        }
        return register(set_user_logged_here);
    }

    /**
     * 
     * @param set_user_logged_here - nome dell'utente loggato o registrato
     * @return - Se avviene una registrazione viene restituito il valore true, poiche solo gli utenti possono registrarsi. Se viene effettuato un login, viene restituito il tipo di utente appena loggato.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected static boolean login(LoggedUser set_user_logged_here) throws  FileNotFoundException,IOException,ClassNotFoundException{

        Scanner reader = new Scanner(System.in);
        String name;
        String password;

        System.out.print("Username: ");
        name = reader.nextLine();
        System.out.print("Password: ");
        password = reader.nextLine();
    
        for (; !(search_username (name,password)) ;){
            System.out.println("Username e/o password non trovati. Vuoi Registrarti ?: Y/N");

            String choice;
            choice = reader.nextLine();
            for ( ;  !(choice.equals("Y")) && !(choice.equals("N"))  ;){
                System.out.println("Scelta Errata. Riprovare");
                choice = reader.nextLine();
            }

            if  (choice.equals("Y")){
                return register(set_user_logged_here);
            }
            System.out.println("Reinserisci i dati di Login ");

            System.out.print("Username: ");
            name = reader.nextLine();
            System.out.print("Password: ");
            password = reader.nextLine();
        }

      set_user_logged_here.logged_user = name;
        System.out.println("Logged");
      return getAccountType(name);

    }

    /**
     * 
     * @param set_user_logged_here - nome dell'utente loggato o registrato
     * @return - true poiche' e' possibile la registrazione solo degli utenti. 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected static boolean register ( LoggedUser set_user_logged_here ) throws  FileNotFoundException,IOException,ClassNotFoundException{

        Scanner reader = new Scanner(System.in);
        String name;
        String password;
        String location;
        System.out.println("---------- Registrazione ----------");
        System.out.println("Inserisci i dati di Registrazione");
        System.out.print("Username: ");
        name = reader.nextLine();
        System.out.print("Password: ");
        password = reader.nextLine();
        System.out.print("Location: ");
        location = reader.nextLine();


        store_data(name,password,location);
        System.out.println("Registrato e login effettuato");

        set_user_logged_here.logged_user = name;
        return true;

    }

    /**
     * Questo metodo si occupa della ricerca dei dati dell'utente all'interno del "database".
     *
     * @param in_name - username dell'utente
     * @param in_password - password dell'utente
     * @return  true se la ricerca ha avuto esito, false altrimenti
     */
    protected static boolean search_username (String in_name, String in_password) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (
                        database.get(i).getUser().equals( in_name) &&
                        database.get(i).getPassword().equals(in_password)
                    )
                {
                 reader.close();
                 return true;
                }
            }
            reader.close();
            return false;

        }else{
            ArrayList<Entry> database = new ArrayList<Entry>();
            database.add(new Entry("admin","admin",false,"UNISA"));

            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();

            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (
                        database.get(i).getUser().equals( in_name) &&
                                database.get(i).getPassword().equals(in_password)
                        )
                {
                    reader.close();
                    return true;
                }
            }
            reader.close();
            return false;
        }
    }

    /**
     * Questo metodo si occupa di restituite la tipologia dell'account dell'utente connesso
     *
     * @param in_name - username utente
     * @return   tipo di account (admin o Client)
     */
    protected static boolean getAccountType(String in_name) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  (  database.get(i).getUser().equals( in_name)   )
                {
                    reader.close();
                    return database.get(i).getType();

                }
            }
            reader.close();
        }
        return false;
    }

    /**
     * Questo metodo si occupa di salvare i dati su disco dell'utente.
     *
     * @param in_nome - username utente
     * @param in_password - password utente
     */
    protected static void store_data (String in_nome, String in_password, String in_location) throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
        if (  (f_data.exists())){
            //Aggiungi dati al database
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            database.add(new Entry(in_nome,in_password,true,in_location));
            reader.close();
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();
        }else{
           //Crea nuovo database e poi memorizza
            ArrayList<Entry> database = new ArrayList<Entry>();
            database.add(new Entry("admin","admin",false,"UNISA"));
            database.add(new Entry(in_nome,in_password,true,in_location));

            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
            writer.writeObject(database);
            writer.close();
        }
    }

    /**
     * Questo emtodo si occupa di prelevare i dati dell'utente dal disco
     * 
     * @param in_nome - username utente
     * @return un Entry contenente tutti i dati dell'utente
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static  Entry get_user_data (String in_nome)throws  FileNotFoundException,IOException,ClassNotFoundException{

        File f_data = new File("user_db");
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
            for (int i = 0 ; i < database.size();++i){
                if  ( database.get(i).getUser().equals( in_nome)  )
                {
                    reader.close();
                    return database.get(i);
                }
            }
        return null; // Non puoi essere qui o altriemnti il tuo database e' corrotto
    }

    /**
     * Questo metodo scrive su disco le modifiche apportate agli attributi di un utente. (Ad esempio bilancio incrementato)
     * 
     * @param in_nome - username utente
     * @param new_entry - Oggetto di tipo Entry
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void replace_data (String in_nome, Entry new_entry )throws  FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("user_db");
        ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
        ArrayList<Entry> database = (ArrayList<Entry>) reader.readObject();
        for (int i = 0 ; i < database.size();++i){
            if  ( database.get(i).getUser().equals( in_nome)  )
            {
                reader.close();
                database.set(i,new_entry);
                break;

            }
        }
        ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
        writer.writeObject(database);
        writer.close();
    }
}
