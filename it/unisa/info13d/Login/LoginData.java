package it.unisa.info13d.Login;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 17/12/13
 * Time: 1.47
 * 
 * Questa classe si occupa della gestione del login dell'utente e dell'amministratore
 *
 *
 */
public class LoginData {

	/**
	 * Il costruttore si occupa di prelevare i dati dell'utente appena loggato. Tipo di account e username
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    public LoginData () throws FileNotFoundException,IOException,ClassNotFoundException{

        LoggedUser logged_user = new LoggedUser();
        boolean AccountType = true;
        AccountType = Access.get_access(logged_user); //chiama al form di login e/o registrazione

        type = convert_type (AccountType);
        username = logged_user.logged_user;
    }

    /**
     * Questo metodo si occupa di convertire un valore bolleano nel tipo di account dll'utente connesso
     * 
     * @param value - tipo di account (Admin o Client) ottenuto dalla chiamata alla funzione Access.get_access(LoggedUser set_user_logged_here)
     * @return restituisce un valore booleano, che rappresenta il tipo di utente connesso, false=>Admin true=>Client
     */
    protected String convert_type (boolean value){
        if (value == false)
            return "Admin";
            return "Client";
    }

    /**
     * @return restituisce l'username dell'utente connesso
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * @return restituisce il tipo di account associato all'utente connesso
     */
    public String getType() {
        return type;
    }
    
    /**
     * Stampa le informazioni dell'utente connesso (username e tipo di account)
     */
    public void getUserDataInfo (){

        System.out.println("\n***************************************");
        System.out.println("Utente Loggato : " + username);
        System.out.println("AccoutType : " + type );
        System.out.println("***************************************\n");
    }

    private String username;
    private String type;
}
