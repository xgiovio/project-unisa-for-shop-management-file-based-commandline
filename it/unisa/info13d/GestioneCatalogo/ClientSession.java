package it.unisa.info13d.GestioneCatalogo;

import it.unisa.info13d.Articoli.Utilizzabile;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.Entry;
import it.unisa.info13d.Utility.ReShow;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Created with PC-STATION.
 * User: lebon
 * Date: 17/12/13
 * Time: 11.10
 */
public class ClientSession {
	/**
	 * Questo metodo visualizza il menu per i Clienti
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public static void showClientMenu(Catalogo catalogo, ReShow r, String username) throws FileNotFoundException, ClassNotFoundException, IOException {
		Entry loggedUser = Access.get_user_data(username);
		System.out.println("------------ Menu operazioni ------------");
		System.out.println("-- Salto totale:"+loggedUser.getBalance());
		System.out.println("1 --> Acquista Credito");       //Si acquista credito per l'acquisto dei prodotti
		System.out.println("2 --> Visualizza Offerte");     //Visualizza le offerte acquistabili
		System.out.println("3 --> Acquista");               //Acquisto di un offerta
		System.out.println("4 --> Storico acquisti");       //Visualizza lo storico degli acquisti dell'utente
		System.out.println("5 --> Esci"); 
		
		String sceltaMenu;
		System.out.print("Operazione: ");
		Scanner inputData = new Scanner(System.in);
		sceltaMenu = inputData.nextLine();
		//Controllo input. La scelta deve essere obbligatoriamente compresa tra 1 e 4
		for ( ;  !(sceltaMenu.equals("1"))&&!(sceltaMenu.equals("2"))&&!(sceltaMenu.equals("3"))&&!(sceltaMenu.equals("4"))&&!(sceltaMenu.equals("5"))  ;){
            System.out.println("Scelta Errata. Riprovare");
            System.out.print("Operazione: ");
            sceltaMenu = inputData.nextLine();
        }
		
		switch(sceltaMenu)
		{
			case "1":
				catalogo.aggiungiCredito(username);
				break;
			case "2":
				catalogo.offerteAttive(username);
				break;
			case "3":
				catalogo.acquistaProdotto(username);
				break;
			case "4":
				catalogo.visualizzaStorico(username);
				break;
			case "5":
                r.reshow = false;
				break;
		}	
	}
}
