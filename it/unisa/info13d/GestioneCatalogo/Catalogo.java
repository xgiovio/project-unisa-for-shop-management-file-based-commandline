package it.unisa.info13d.GestioneCatalogo;
import java.util.*;

import it.unisa.info13d.Articoli.BeniDiConsumo;
import it.unisa.info13d.Articoli.CeneInRistoranti;
import it.unisa.info13d.Articoli.Global;
import it.unisa.info13d.Articoli.Utilizzabile;
import it.unisa.info13d.Articoli.Vacanze;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.Entry;
import it.unisa.info13d.Utility.IdCounter;
import it.unisa.info13d.Utility.ReShow;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoggedUser;
import it.unisa.info13d.Login.LoginData;
import it.unisa.info13d.Login.StoricoItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created with PC-STATION.
 * User: lebon
 * Date: 17/12/13
 * Time: 10.45
 */
public class Catalogo{

	/**
	 * Il costruttore definisce un nuovo catalogo nel caso non sia gia presente.
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public Catalogo() throws FileNotFoundException,IOException,ClassNotFoundException
	{
        File f_data = new File("cat_db");
        if (  (f_data.exists())){
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(f_data));
            catalogoOfferte = (ArrayList<Utilizzabile>) reader.readObject();
            reader.close();
        } else {
            catalogoOfferte = new ArrayList<Utilizzabile>();
        }
    }

	/**
	 * Questo metodo si occupa di serializzare il catalogo (per garantire un minimo di sicurezza) e salvarlo su disco
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
    void store_data()throws FileNotFoundException,IOException,ClassNotFoundException{
        File f_data = new File("cat_db");
        ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(f_data));
        writer.writeObject(catalogoOfferte);
        writer.close();
    }

	
	/**
	 * Questo metodo seleziona in Menu da visualizzare in base al login effettuato dall'utente
	 * 
	 * @param userType is the type of user obtained from the main
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public void showMenu(String userType,String username, ReShow r) throws FileNotFoundException, ClassNotFoundException, ParseException, IOException
	{
		if(userType.equals("Admin"))
			AdminSession.showAdminMenu(this,r, username);
		else
			ClientSession.showClientMenu(this,r, username);
	}
	
	/**
	 * Questo metodo permette l'inserimento di un nuovo deal all'interno del catalogo
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public void nuovoProdotto() throws ParseException, FileNotFoundException, ClassNotFoundException, IOException
	{
		System.out.println("--------- Aggiunta nuovo Prodotto ----------");
		System.out.println("1 --> Vacanza");
		System.out.println("2 --> Cena");
		System.out.println("3 --> Bene di consumo ");
		
		String sceltaMenu;
		System.out.print("Prodotto: ");
		Scanner inputData = new Scanner(System.in);
		sceltaMenu = inputData.nextLine();
		//Controllo input. La scelta deve essere obbligatoriamente compresa tra 1 e 4
		for ( ;  !(sceltaMenu.equals("1"))&&!(sceltaMenu.equals("2"))&&!(sceltaMenu.equals("3")) ; ){
            System.out.println("Scelta Errata. Riprovare");
            System.out.print("Prodotto: ");
            sceltaMenu = inputData.nextLine();
        }
		
		switch(sceltaMenu)
		{
			case "1":
				System.out.println("---------- Nuova Offerta Vacanza ----------");
				System.out.print("Localita: ");
				String localita = inputData.nextLine();
				
				System.out.print("Data Partenza (GG/MM/AAAA): ");
				 String data = inputData.nextLine();
				 String temp[];
				 String marcatore="/";
				 temp = data.split(marcatore);
				 GregorianCalendar dataPartenza = new GregorianCalendar((Integer.parseInt(temp[2])),(Integer.parseInt(temp[1]))-1,(Integer.parseInt(temp[0])));
							
				System.out.print("Data Scadenza offerta (GG/MM/AAAA): ");
				 String data2 = inputData.nextLine();
				 String temp2[];
				 temp2 = data2.split(marcatore);
				 GregorianCalendar dataScad = new GregorianCalendar((Integer.parseInt(temp2[2])),(Integer.parseInt(temp2[1]))-1,(Integer.parseInt(temp2[0])));
								
				System.out.print("Prezzo singola persona: ");
				String prezzo = inputData.nextLine();
				double prezzoPSing = Double.parseDouble(prezzo);
								
				catalogoOfferte.add(new Vacanze(Global.get_next_id(), localita, dataPartenza, dataScad, prezzoPSing));
				store_data();
				break;
			case "2":
				System.out.println("---------- Nuova Offerta Cena ----------");
				System.out.print("Ristorante: ");
				String ristorante = inputData.nextLine();
				
				System.out.print("Localita: ");
				String localitaCena = inputData.nextLine();
				
				System.out.print("Descrizione: ");
				String descrizioneCena = inputData.nextLine();
				
				System.out.print("Costo a Persona: ");
				String prezzoCena = inputData.nextLine();
				double prezzoPSingCena = Double.parseDouble(prezzoCena);
				
				System.out.print("Data Scadenza offerta (GG/MM/AAAA): ");
				 String data3 = inputData.nextLine();
				 String temp3[];
				 String marcatore2="/";
				 temp3 = data3.split(marcatore2);
				 GregorianCalendar dataScadCena = new GregorianCalendar((Integer.parseInt(temp3[2])),(Integer.parseInt(temp3[1]))-1,(Integer.parseInt(temp3[0])));
							
				System.out.print("Cene da Vendere: ");
				String nCene = inputData.nextLine();
				int ceneDaVendere = Integer.parseInt(nCene);
			
				catalogoOfferte.add(new CeneInRistoranti(Global.get_next_id(), ristorante, localitaCena, descrizioneCena, prezzoPSingCena, dataScadCena, ceneDaVendere));
				store_data();
				break;
			case "3":
				System.out.println("---------- Nuova Offerta Bene ----------");
				System.out.print("Descrizione: ");
				String descrizioneBene = inputData.nextLine();
				
				System.out.print("Prezzo Bene: ");
				String prezzoBene = inputData.nextLine();
				int prezzo_bene = Integer.parseInt(prezzoBene);
				
				System.out.print("Quantita in magazzino: ");
				String qntMag = inputData.nextLine();
				int qnt_mag = Integer.parseInt(prezzoBene);
								
				catalogoOfferte.add(new BeniDiConsumo(Global.get_next_id(), descrizioneBene, prezzo_bene, qnt_mag));
				store_data();
				break;
		}
        IdCounter.save_counter();
	}
	
	/**
	 * Questo metodo permette la cancellazione di un prodotto dal catalogo
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void cancellaProdotto() throws FileNotFoundException, ClassNotFoundException, IOException
	{
		System.out.println("------ Lista Prodotti -----\n--");
		for(Utilizzabile deal: catalogoOfferte)
		{
			if(deal instanceof Vacanze)
			{
				Vacanze dealVacanza = (Vacanze)deal;
				visualizzaVacanza(dealVacanza);
				
			}
			else if(deal instanceof BeniDiConsumo)
			{
				BeniDiConsumo dealBene = (BeniDiConsumo)deal;
				visualizzaBene(dealBene);
			}
			else if(deal instanceof CeneInRistoranti)
			{
				CeneInRistoranti dealCena = (CeneInRistoranti)deal;
				visualizzaCena(dealCena);
			}
		}
		
		System.out.print("Elimina prodotto [Inserisci id]: ");
		Scanner inputData = new Scanner(System.in);
		String id = inputData.nextLine();
		int idDel = Integer.parseInt(id);
		System.out.println(idDel);
		for(Utilizzabile deal: catalogoOfferte)
		{
			if(deal instanceof Vacanze && ((Vacanze)deal).getIdViaggio()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
				break;
			}
			if(deal instanceof BeniDiConsumo && ((BeniDiConsumo) deal).getIdBene()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
				break;
			}
			if(deal instanceof CeneInRistoranti && ((CeneInRistoranti) deal).getIdCena()==idDel)
			{
				catalogoOfferte.remove(deal);
				store_data();
				break;
			}
		}		
	}

	/**
	 *
     * Questo metodo velocizza la stampa dell informazioni scorrenndo l'array.
     *
	 * @param a
	 */
    public void print_cat_admin (ArrayList<Utilizzabile> a){
        for(Utilizzabile deal: a)
        {
            if(deal instanceof Vacanze)
            {
                Vacanze dealVacanza = (Vacanze)deal;
                if((dealVacanza.eAcquistabile()))
                {
                    visualizzaVacanza(dealVacanza);
                }
                continue;
            }
            if(deal instanceof BeniDiConsumo)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)deal;
                if((dealBene.eAcquistabile()))
                {
                    visualizzaBene(dealBene);
                }
                continue;
            }
            if(deal instanceof CeneInRistoranti)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)deal;

                if((dealCena.eAcquistabile()))
                {
                    visualizzaCena(dealCena);
                }
                continue;
            }
        }
    }

    /**
     *  Stamapa la lista degli oggetti utilizzabile escludendo le cene con location diversa da quelle dell'utente
     *
     * @param a
     * @param location_user_to_compare
     */
    public void print_cat_user (ArrayList<Utilizzabile> a, String location_user_to_compare){
        for(Utilizzabile deal: a)
        {
            if(deal instanceof Vacanze)
            {
                Vacanze dealVacanza = (Vacanze)deal;
                if((dealVacanza.eAcquistabile()))
                {
                    visualizzaVacanza(dealVacanza);
                }
                continue;
            }
            if(deal instanceof BeniDiConsumo)
            {
                BeniDiConsumo dealBene = (BeniDiConsumo)deal;
                if((dealBene.eAcquistabile()))
                {
                    visualizzaBene(dealBene);
                }
                continue;
            }
            if(deal instanceof CeneInRistoranti)
            {
                CeneInRistoranti dealCena = (CeneInRistoranti)deal;

                if((dealCena.eAcquistabile() && dealCena.getLuogo().equals(location_user_to_compare )))
                {
                    visualizzaCena(dealCena);
                }
                continue;
            }
        }
    }

    /**
     * 
     * @param in - data da convertire in millisecondi
     * @return - il numero in millisencodi della data passata alla funzione.
     */
    protected long convert_calendar_to_int (GregorianCalendar in){
               return in.getTimeInMillis();
    }

	/**
	 * Questo metodo visualizza le offerte attive del catalogo. Viene differenziata la visualizzazione in base all'utente.
	 * - L'amministratore avra' la possibilita' di scegliere l'ordinamento in base dalla data di scadenza o ID prodotti
	 * - L'utente visualizzera' le offerte attive senza possibilita' di ordinamento
	 * 
	 * @param user - username utente
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void offerteAttive(String user) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		if(!(loggedUser.getType()))
		{
            Scanner reader = new Scanner(System.in);


            System.out.println("1 --> Ordina Per Data di Scadenza");
            System.out.println("2 --> Ordina Per ID");

            String choice;
            System.out.print("Scelta operazione: ");
            choice = reader.nextLine();
            for ( ;  !(choice.equals("1")) && !(choice.equals("2"))  ;){
                System.out.println("Scelta Errata. Riprovare");
                System.out.print("Scelta operazione: ");
                choice = reader.nextLine();
            }

            if  (choice.equals("1")){

                class CompareDataScadenza implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                      if (e1 instanceof BeniDiConsumo){
                           //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                    return -1;
                                    return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena

                                    if (((BeniDiConsumo)e1).getScadenza().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                        return -1;
                                    return 1;
                                }
                            }
                      }else{
                          if (e1 instanceof Vacanze){
                               //vacanze vs
                              if (e2 instanceof BeniDiConsumo){
                                  //vacanze vs bene
                                  if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                      return -1;
                                  return 1;

                              }else{
                                  if (e2 instanceof Vacanze){
                                      //vacabnza vs vacanza

                                      if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                          return -1;
                                      return 1;


                                  }else{
                                      //vacanza vs cena

                                      if (((Vacanze)e1).getScadenzaOfferta().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                          return -1;
                                      return 1;

                                  }
                              }

                          }else{
                              //Cene vs
                              if (e2 instanceof Vacanze){
                                  //cena vs vacanza

                                  if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((Vacanze)e2).getScadenzaOfferta().getTimeInMillis())
                                      return -1;
                                  return 1;

                              }else{
                                  if (e2 instanceof CeneInRistoranti){
                                      //cena vs cena

                                      if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((CeneInRistoranti)e2).getDataScadenzaOffertaCena().getTimeInMillis())
                                          return -1;
                                      return 1;
                                  }else{
                                      //cena vs bene

                                      if (((CeneInRistoranti)e1).getDataScadenzaOffertaCena().getTimeInMillis() < ((BeniDiConsumo)e2).getScadenza().getTimeInMillis())
                                          return -1;
                                      return 1;
                                  }
                              }

                          }
                      }

                    }
                }

                Collections.sort(catalogoOfferte,new CompareDataScadenza());
                print_cat_admin (catalogoOfferte);
            }else{

                class CompareID implements Comparator<Utilizzabile> {

                    @Override
                    public int compare(Utilizzabile e1, Utilizzabile e2) {

                        if (e1 instanceof BeniDiConsumo){
                            //beni vs
                            if (e2 instanceof Vacanze){
                                //beni vs vacanze
                                if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof BeniDiConsumo){
                                    //bene vs //bene

                                    if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    //bene vs cena

                                    if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;
                                }
                            }
                        }else{
                            if (e1 instanceof Vacanze){
                                //vacanze vs
                                if (e2 instanceof BeniDiConsumo){
                                    //vacanze vs bene
                                    if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof Vacanze){
                                        //vacabnza vs vacanza

                                        if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                            return -1;
                                        return 1;


                                    }else{
                                        //vacanza vs cena

                                        if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;

                                    }
                                }

                            }else{
                                //Cene vs
                                if (e2 instanceof Vacanze){
                                    //cena vs vacanza

                                    if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;

                                }else{
                                    if (e2 instanceof CeneInRistoranti){
                                        //cena vs cena

                                        if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                            return -1;
                                        return 1;
                                    }else{
                                        //cena vs bene

                                        if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                            return -1;
                                        return 1;
                                    }
                                }

                            }
                        }

                    }
                }


                Collections.sort(catalogoOfferte,new CompareID());
                print_cat_admin (catalogoOfferte);

            }


		} else{

            class CompareIDUser implements Comparator<Utilizzabile> {

                @Override
                public int compare(Utilizzabile e1, Utilizzabile e2) {

                    if (e1 instanceof BeniDiConsumo){
                        //beni vs
                        if (e2 instanceof Vacanze){
                            //beni vs vacanze
                            if (((BeniDiConsumo)e1).getIdBene() < ((Vacanze)e2).getIdViaggio())
                                return -1;
                            return 1;

                        }else{
                            if (e2 instanceof BeniDiConsumo){
                                //bene vs //bene

                                if (((BeniDiConsumo)e1).getIdBene() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                //bene vs cena

                                if (((BeniDiConsumo)e1).getIdBene() < ((CeneInRistoranti)e2).getIdCena())
                                    return -1;
                                return 1;
                            }
                        }
                    }else{
                        if (e1 instanceof Vacanze){
                            //vacanze vs
                            if (e2 instanceof BeniDiConsumo){
                                //vacanze vs bene
                                if (((Vacanze)e1).getIdViaggio() < ((BeniDiConsumo)e2).getIdBene())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof Vacanze){
                                    //vacabnza vs vacanza

                                    if (((Vacanze)e1).getIdViaggio() < ((Vacanze)e2).getIdViaggio())
                                        return -1;
                                    return 1;


                                }else{
                                    //vacanza vs cena

                                    if (((Vacanze)e1).getIdViaggio() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;

                                }
                            }

                        }else{
                            //Cene vs
                            if (e2 instanceof Vacanze){
                                //cena vs vacanza

                                if (((CeneInRistoranti)e1).getIdCena() < ((Vacanze)e2).getIdViaggio())
                                    return -1;
                                return 1;

                            }else{
                                if (e2 instanceof CeneInRistoranti){
                                    //cena vs cena

                                    if (((CeneInRistoranti)e1).getIdCena() < ((CeneInRistoranti)e2).getIdCena())
                                        return -1;
                                    return 1;
                                }else{
                                    //cena vs bene

                                    if (((CeneInRistoranti)e1).getIdCena() < ((BeniDiConsumo)e2).getIdBene())
                                        return -1;
                                    return 1;
                                }
                            }

                        }
                    }

                }
            }


            Collections.sort(catalogoOfferte,new CompareIDUser());
            print_cat_user(catalogoOfferte,loggedUser.getLocation());

        }
	}
	
	/**
	 * Questo metodo visualizza le offerte scadute del catalogo
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public void offerteScadute(String user) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		System.out.println("------ Offerte Scadute -----");
		for(Utilizzabile deal: catalogoOfferte)
		{
			if(deal instanceof Vacanze)
			{
				Vacanze dealVacanza = (Vacanze)deal;
				if(!(dealVacanza.eAcquistabile())) {visualizzaVacanza(dealVacanza);continue;}
			}
			if(deal instanceof BeniDiConsumo)
			{
				BeniDiConsumo dealBene = (BeniDiConsumo)deal;
				if(!(dealBene.eAcquistabile())) {visualizzaBene(dealBene);continue;}
			}
			if(deal instanceof CeneInRistoranti)
			{
				CeneInRistoranti dealCena = (CeneInRistoranti)deal;
				if(!(dealCena.eAcquistabile())) {visualizzaCena(dealCena);continue;}
			}
		}
	}
	/**
	 * Questa metodo aggiunge credito al conto dell'utente
	 * @param user
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void aggiungiCredito(String user) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		System.out.println("----- Acquisto Credito -----");
		System.out.println("Saldo Attuale:"+ loggedUser.getBalance());
		System.out.println("Aggiungi importo da ricaricare:");
		Scanner inputData = new Scanner(System.in);
		String euro = inputData.nextLine();
		loggedUser.setBalance(Double.parseDouble(euro));
		Access.replace_data(user, loggedUser);
		System.out.println("Saldo attuale: "+loggedUser.getBalance());
	}

	/**
	 * Questo metodo permette all'utente di effettuare un acquisto
	 * 
	 * @param user
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void acquistaProdotto(String user) throws FileNotFoundException, ClassNotFoundException, IOException
	{
		Entry loggedUser = Access.get_user_data(user);
		System.out.println("----- Shop ----");
		System.out.println("### Prodotti ###");
		
		offerteAttive(user);
		
		System.out.println("Offerta da acquistare[inserisci id]: ");
		Scanner inputData = new Scanner(System.in);
		String idProd = inputData.nextLine();
		int idDel = Integer.parseInt(idProd);
		
		boolean trovato=false;
		ArrayList<StoricoItem> storico = loggedUser.getStorico();
		
		for(Utilizzabile dealsc: catalogoOfferte)
		{
			if((dealsc instanceof Vacanze) && ((Vacanze)dealsc).getIdViaggio()==idDel)
			{
				Vacanze dealVacanza = (Vacanze)dealsc;
				if(dealVacanza.eAcquistabile() && loggedUser.getBalance()>=dealVacanza.getPrezzoPSingola())
				{
					trovato=true;
					storico.add(new StoricoItem("Viaggio: "+dealVacanza.getLocalitaViaggio(), new GregorianCalendar() ,dealVacanza.getPrezzoPSingola())); 
					dealVacanza.setViaggiVenduti();
					loggedUser.setBalance((dealVacanza.getPrezzoPSingola() * (-1)));
					store_data();
					Access.replace_data(user, loggedUser);
					System.out.println("************************************\nAcquisto completato con successo! \n************************************\nSaldo Attuale:" + loggedUser.getBalance());
				}
				else
				{
					System.out.println("Il prodotto non e' acquistabile e/o Credito insufficiente, ricaricare");
					trovato=true;
					break;
				}	
			}
			if((dealsc instanceof BeniDiConsumo) && ((BeniDiConsumo) dealsc).getIdBene()==idDel)
			{
				BeniDiConsumo dealBene = (BeniDiConsumo)dealsc;
				if(dealBene.eAcquistabile() && loggedUser.getBalance()>=dealBene.getPrezzoBene())
				{
					trovato=true;
					storico.add(new StoricoItem("Prodotto: "+dealBene.getDescrizioneBene(), new GregorianCalendar() ,dealBene.getPrezzoBene()));
					dealBene.setBeniVenduti();
					loggedUser.setBalance((dealBene.getPrezzoBene() * (-1)));
					Access.replace_data(user, loggedUser);
					store_data();
					System.out.println("************************************\nAcquisto completato con successo! \n************************************\nSaldo Attuale:" + loggedUser.getBalance());
				}
				else
				{
					System.out.println("Il prodotto non e' acquistabile e/o Credito insufficiente, ricaricare");
					trovato=true;
					break;
				}
				
			}
			if( (dealsc instanceof CeneInRistoranti) && ((CeneInRistoranti) dealsc).getIdCena()==idDel)
			{
				CeneInRistoranti dealCena = (CeneInRistoranti)dealsc;
				if(dealCena.eAcquistabile() && loggedUser.getBalance()>=dealCena.getCostoPerPersona())
				{
					trovato=true;
					storico.add(new StoricoItem("Cena ristorante "+dealCena.getNomeRistorante()+" "+dealCena.getDescrizione_cena(), new GregorianCalendar() ,dealCena.getCostoPerPersona()));
					dealCena.setCeneVendute();
					loggedUser.setBalance((dealCena.getCostoPerPersona() * (-1)));
					Access.replace_data(user, loggedUser);
					store_data();
					System.out.println("*************************************\nAcquisto completato con successo! \n************************************\nSaldo Attuale:" + loggedUser.getBalance());
				}
				else
				{
					System.out.println("Il prodotto non e' acquistabile e/o Credito insufficiente, ricaricare");
					trovato=true;
					break;
				}
			}
		}
		if(!trovato) System.out.println("### Prodotto non presente nel catalogo");
	}	
	
	/**
	 * Questo metodo visualizza lo storico degli ordini effettuati dall'utente. rende possibile l'ordinamento in base alla data di scadenza o per prezzo.
	 * 
	 * @param user
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void visualizzaStorico(String user) throws FileNotFoundException, ClassNotFoundException, IOException
	{
        Scanner reader = new Scanner(System.in);

        System.out.println("1 --> Ordina Per Data di Acquisto");
        System.out.println("2 --> Ordina Per Costo Prodotto");

        String choice;
        System.out.print("Opzione: ");
        choice = reader.nextLine();
        for ( ;  !(choice.equals("1")) && !(choice.equals("2"))  ;){
            System.out.println("Scelta Errata. Riprovare");
            System.out.print("Opzione: ");
            choice = reader.nextLine();
        }

        Entry userLogged = Access.get_user_data(user);
        System.out.println("---------- STORICO ----------");
        ArrayList<StoricoItem> storico = userLogged.getStorico();

        if  (choice.equals("1")){
            class CompareDataAcquisto implements Comparator<StoricoItem> {

                @Override
                public int compare(StoricoItem e1, StoricoItem e2) {

                    if (e1.getData_acquisto().getTimeInMillis()  < e2.getData_acquisto().getTimeInMillis())
                        return -1;
                    return 1;
                }
            }

            Collections.sort(storico,new CompareDataAcquisto());
            print_storico(storico);

        } else {

            class ComparePrezzo implements Comparator<StoricoItem> {

                @Override
                public int compare(StoricoItem e1, StoricoItem e2) {

                    if (e1.getPrezzo()  < e2.getPrezzo())
                        return -1;
                    return 1;
                }
            }
            Collections.sort(storico,new ComparePrezzo());
            print_storico (storico);
        }
	}

    protected void print_storico (ArrayList<StoricoItem> in){

        for(StoricoItem record: in)
        {
            GregorianCalendar partenza = record.getData_acquisto();
            int giornoP = partenza.get(Calendar.DAY_OF_MONTH);
            int meseP = partenza.get(Calendar.MONTH);
            int annoP = partenza.get(Calendar.YEAR);
            System.out.println("| deal: "+record.getDescription()+"| Data Acquisto: "+giornoP+"/"+(meseP+1)+"/"+annoP+"| Prezzo: "+record.getPrezzo());
        }

    }
	
	
    /**
	 * Questo metodo visualizza una vacanza, descrivendone i suoi dati.
	 * 
	 * @param dealVacanza - oggetto di tipo Vacanze che rappresenta l'offerta della Vacanza da visualizzare
	 */
    public void visualizzaVacanza(Vacanze dealVacanza)
	{
		GregorianCalendar partenza = dealVacanza.getDataPartenzaViaggio();
		int giornoP = partenza.get(Calendar.DAY_OF_MONTH);
        int meseP = partenza.get(Calendar.MONTH);
        int annoP = partenza.get(Calendar.YEAR);
		GregorianCalendar scadenza = dealVacanza.getScadenzaOfferta();
		int giornoS = scadenza.get(Calendar.DAY_OF_MONTH);
        int meseS = scadenza.get(Calendar.MONTH);
        int annoS = scadenza.get(Calendar.YEAR);
		
		System.out.println("#Vacanza-"+dealVacanza.getIdViaggio()+"# Scadenza: "+giornoS+"/"+(meseS+1)+"/"+annoS+"| Localita: "+dealVacanza.getLocalitaViaggio()+"| Partenza: "+giornoP+"/"+(meseP+1)+"/"+annoP+"| Prezzo Pers. Singola: "+dealVacanza.getPrezzoPSingola()+"\n--");
	}
   
    /**
     *  Questo metodo visualizza un Bene di consumo, descrivendone i suoi dati.
     *
     * @param dealBene - oggetto di tipo beniDiConsumo che rappresenta l'offerta del bene da visualizzare
     */
	
	public void visualizzaBene(BeniDiConsumo dealBene)
	{
		System.out.println("#Articolo-"+dealBene.getIdBene()+"# Descrizione: "+dealBene.getDescrizioneBene()+" Prezzo: "+dealBene.getPrezzoBene()+"| Quantita in magazzino: "+dealBene.getBeniInStock()+"\n--");
	}

	
	/**
	 * 
	 * Questo metodo visualizza una cena, descrivendone i suoi dati.
	 * @param dealCena - oggetto di tipo CeneInRistoranti che rappresenta l'offerta della cena da visualizzare
	 */
	public void visualizzaCena(CeneInRistoranti dealCena)
	{
		GregorianCalendar scadenzaCena = dealCena.getDataScadenzaOffertaCena();
		int giornoSC = scadenzaCena.get(Calendar.DAY_OF_MONTH);
        int meseSC = scadenzaCena.get(Calendar.MONTH);
        int annoSC = scadenzaCena.get(Calendar.YEAR);
        System.out.println("#Cena-"+dealCena.getIdCena()+"# Scadenza:"+giornoSC+"/"+(meseSC+1)+"/"+annoSC+"| Ristorante: "+dealCena.getNomeRistorante()+"| Indirizzo: "+dealCena.getLuogo()+"| Descrizione: "+dealCena.getDescrizione_cena()+"| Costo: "+dealCena.getCostoPerPersona()+"| Disponibilita:"+dealCena.getNumCenedaVendere()+"cene"+"\n--");
	}
	
	/**
	 * 
	 * @return - restituisce il catalogo delle offerte
	 */
   
	public ArrayList<Utilizzabile> getCatalogo() {
        return catalogoOfferte;
    }
		


    private ArrayList<Utilizzabile> catalogoOfferte;
  
}
