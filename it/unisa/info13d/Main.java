package it.unisa.info13d;

import it.unisa.info13d.GestioneCatalogo.Catalogo;
import it.unisa.info13d.Login.Access;
import it.unisa.info13d.Login.LoggedUser;
import it.unisa.info13d.Login.LoginData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import it.unisa.info13d.Utility.IdCounter;
import it.unisa.info13d.Utility.ReShow;
/**
 * Questa classe rappresenta il Main dell'applicazione dalla quale si avvieranno le procedure per la gestione dell'utente e del catalogo.
 * 
 * @author Simone Argenziano e Giovanni Di Grezia
 * 
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException,IOException,ClassNotFoundException, ParseException {

        LoginData login = new LoginData();
        login.getUserDataInfo();

        IdCounter.restore_counter();
        Catalogo load_catalogo = new Catalogo();

        ReShow r = new ReShow();
        for (;r.reshow;)
            load_catalogo.showMenu(login.getType(),login.getUsername(),r);
    }
}
