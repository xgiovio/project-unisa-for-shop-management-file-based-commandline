package it.unisa.info13d.Articoli;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.47
 */

/**
 * 
 * Classe che rappresenta una cena all'interno del catalogo.
 *
 */
public class CeneInRistoranti implements Utilizzabile, Serializable{

    /**
     *
     * @param idCena - l'id della cena
     * @param nomeRistorante - il ristorante in cui usufruire dell'offerta 
     * @param luogo - Ubicazione del ristorante
     * @param descrizione_cena - descrizione della cena (cosa viene offerto dal ristorante)
     * @param costoPerPersona - costo per singola persona 
     * @param dataScadenzaOffertaCena - scadenza dell'offerta
     * @param numCenedaVendere - offerte disponibile per la cena
     */

    public CeneInRistoranti(int idCena, String nomeRistorante, String luogo, String descrizione_cena, double costoPerPersona, GregorianCalendar dataScadenzaOffertaCena, int numCenedaVendere){
    	this.idCena=idCena;
    	this.nomeRistorante=nomeRistorante;
    	this.luogo=luogo;
    	this.descrizione_cena=descrizione_cena;
    	this.costoPerPersona=costoPerPersona;
    	this.dataScadenzaOffertaCena=dataScadenzaOffertaCena;
    	this.numCenedaVendere=numCenedaVendere;
    }

	@Override
	public boolean eAcquistabile() {
		if((dataScadenzaOffertaCena.before(new GregorianCalendar())) && (numCenedaVendere>ceneVendute))
				return false;
		return true;
	}

    /**
     * @return - l'id della cena
     */
    public int getIdCena() {
        return idCena;
    }

    /**
     * @return - in nome del ristorante
     */
    public String getNomeRistorante() {
        return nomeRistorante;
    }

    /**
     * @return - l'ubicazione del ristorante
     */
    public String getLuogo() {
        return luogo;
    }

    /**
     * @return - descrizione della cena
     */
    public String getDescrizione_cena() {
        return descrizione_cena;
    }

    /**
     * @return - il costo per ogni singola persona
     */
    public double getCostoPerPersona() {
        return costoPerPersona;
    }

    /**
     * @return - data di scadenza dell'offerta
     */
    public GregorianCalendar getDataScadenzaOffertaCena() {
        return dataScadenzaOffertaCena;
    }

    /**
     * @return - il numero di cene ancora disponibili per la vendita
     */
    public int getNumCenedaVendere() {
        return numCenedaVendere;
    }

    /**
     * @return - il numero di cene vendute 
     */
    public int getCeneVendute() {
        return ceneVendute;
    }

    /** 
     * @param nomeRistorante  - setta il nome del ristorante
     */
    public void setNomeRistorante(String nomeRistorante) {
        this.nomeRistorante = nomeRistorante;
    }

    /**
     * @param luogo - setta il l'ubicazione del ristorante
     */
    public void setLuogo(String luogo) {
        this.luogo = luogo;
    }

    /**
     * @param descrizione_cena - setta la descrizione della cena (Esempio: pizza margherita, patate e dolce)
     */
    public void setDescrizione_cena(String descrizione_cena) {
        this.descrizione_cena = descrizione_cena;
    }

    /**
     * @param costoPerPersona - setta il costo per singola persona
     */
    public void setCostoPerPersona(double costoPerPersona) {
        this.costoPerPersona = costoPerPersona;
    }

    /**
     * @param dataScadenzaOffertaCena - setta la data di scadenza per l'offerta
     */
    public void setDataScadenzaOffertaCena(GregorianCalendar dataScadenzaOffertaCena) {
        this.dataScadenzaOffertaCena = dataScadenzaOffertaCena;
    }

    /**
     * @param numCenedaVendere - setta il numero di cene da vendere
     */
    public void setNumCenedaVendere(int numCenedaVendere) {
        this.numCenedaVendere = numCenedaVendere;
    }
    
    /**
     * Questo metodo all'atto dell'acquisto da parte di un utente, decrementa le cene da vendere ed incrementa quelle vendute
     */
    public void setCeneVendute()
    {
    	numCenedaVendere--;
    	ceneVendute++;
    }

    private int idCena;
	private String nomeRistorante;
	private String luogo;
	private String descrizione_cena;
	private double costoPerPersona;
	private GregorianCalendar dataScadenzaOffertaCena;
	private int numCenedaVendere;
	private int ceneVendute;
}
