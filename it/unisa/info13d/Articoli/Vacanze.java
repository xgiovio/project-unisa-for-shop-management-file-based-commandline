package it.unisa.info13d.Articoli;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.45
 */
public  class Vacanze implements Utilizzabile, Serializable{


    /**
     * Costruttore per inizialiazzare una Vacanza
     *
     * @param idViaggio
     * @param localitaViaggio
     * @param dataPartenzaViaggio
     * @param scadenzaOfferta
     * @param prezzoPSingola
     */
    public  Vacanze(int idViaggio, String localitaViaggio, GregorianCalendar dataPartenzaViaggio, GregorianCalendar scadenzaOfferta, double prezzoPSingola){
    	this.idViaggio			 = idViaggio;
    	this.localitaViaggio	 = localitaViaggio;
    	this.dataPartenzaViaggio = dataPartenzaViaggio;
    	this.scadenzaOfferta	 = scadenzaOfferta;
    	this.prezzoPSingola		 = prezzoPSingola;
    }

	@Override
	public boolean eAcquistabile() {
		if(scadenzaOfferta.before(new GregorianCalendar())) //L'offerta della vacanza scade se la data sua scadenza e successiva a quella della data odierna
			return false;
		return true;
	}

    /**
     * @return - id del viaggio
     */
    public int getIdViaggio() {
        return idViaggio;
    }

    /**
     * @return - localita' del viaggio
     */
    public String getLocalitaViaggio() {
        return localitaViaggio;
    }

    /**
     * @return - data di partenza
     */
    public GregorianCalendar getDataPartenzaViaggio() {
        return dataPartenzaViaggio;
    }

    /**
     * @return - data di scadenza dell'offerta
     */
    public GregorianCalendar getScadenzaOfferta() {
        return scadenzaOfferta;
    }

    /**
     * @return - il prezzo per singola persona
     */
    public double getPrezzoPSingola() {
        return prezzoPSingola;
    }

    /**
     * @return - il numero di viaggi venduti
     */
    public int getViaggiVenduti() {
        return viaggiVenduti;
    }

    /**
     * @param localitaViaggio - setta la localit' del viaggio
     */
    public void setLocalitaViaggio(String localitaViaggio) {
        this.localitaViaggio = localitaViaggio;
    }

    /**
     * @param dataPartenzaViaggio - setta la data di patenza del viaggio
     */
    public void setDataPartenzaViaggio(GregorianCalendar dataPartenzaViaggio) {
        this.dataPartenzaViaggio = dataPartenzaViaggio;
    }

    /**
     * @param scadenzaOfferta - setta la data di scadenza dell'offerta della vacanza
     */
    public void setScadenzaOfferta(GregorianCalendar scadenzaOfferta) {
        this.scadenzaOfferta = scadenzaOfferta;
    }

    /**
     * @param prezzoPSingola - setta il prezzo per singola persona
     */
    public void setPrezzoPSingola(double prezzoPSingola) {
        this.prezzoPSingola = prezzoPSingola;
    }
    
    /**
     * Questo metodo incrementa, dopo ogni acquisto, il numero di viaggi venduti
     */
    public void setViaggiVenduti()
    {
    	viaggiVenduti++;
    }

    private int idViaggio;
	private String localitaViaggio;
	private GregorianCalendar dataPartenzaViaggio;
	private GregorianCalendar scadenzaOfferta;
	private double prezzoPSingola;
	private int viaggiVenduti;
}
