package it.unisa.info13d.Articoli;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.46
 */

/**
 * 
 * Classe che rappresenta un bene di consumo all'interno del catalogo.
 *
 */
public class BeniDiConsumo implements Utilizzabile, Serializable{

    /**
     * Costruttore che inizializza i valori del bene di consumo
     *
     * @param idBene
     * @param descrizioneBene
     * @param prezzoBene
     * @param beniInStock
     */
    public BeniDiConsumo (  int idBene, String descrizioneBene, double prezzoBene, int beniInStock ){
    	this.idBene=idBene;
    	this.descrizioneBene=descrizioneBene;
    	this.prezzoBene=prezzoBene;
    	this.beniInStock=beniInStock;
    }

	@Override
	public boolean eAcquistabile() {
		if(beniVenduti<beniInStock)
			return true;
		return false;
	}


    /**
     * @return - id bene
     */
    public int getIdBene() {
        return idBene;
    }

    /**
     * @return - descrizione bene
     */
    public String getDescrizioneBene() {
        return descrizioneBene;
    }

    /**
     * @return - prezzo del bene
     */
    public double getPrezzoBene() {
        return prezzoBene;
    }

    /**
     * @return -  la quantita dei beni da vendere
     */
    public int getBeniInStock() {
        return beniInStock;
    }

    /**
     * @return - numero di oggetti venduti
     */
    public int getBeniVenduti() {
        return beniVenduti;
    }

    /**
     * @param descrizioneBene - descrizione del bene
     */
    public void setDescrizioneBene(String descrizioneBene) {
        this.descrizioneBene = descrizioneBene;
    }

    /**
     * @param prezzoBene - prezzo del bene
     */
    public void setPrezzoBene(double prezzoBene) {
        this.prezzoBene = prezzoBene;
    }

    /**
     * @param beniInStock - quantita' del bene disponibile per la vendita
     */
    public void setBeniInStock(int beniInStock) {
        this.beniInStock = beniInStock;
    }

    /**
     * @return - la scadenza del bene (viene considerata una data a lungo termine)
     */
    public GregorianCalendar getScadenza() {
        return scadenza;
    }

    /**
     * Questo metodo setta i beni venduti, decrementa la quantita in magazzino e incrementa il numero di oggeti venduti
     */
    public void setBeniVenduti() {
    	beniInStock--;
    	beniVenduti++;
    }

    private int idBene;
	private String descrizioneBene;
	private double prezzoBene;
	private int beniInStock; //Numero totale di prodotti da vendere
	private int beniVenduti; //Numero di prodotti venduti
    private GregorianCalendar scadenza = new GregorianCalendar(99999,0,1);
}
