package it.unisa.info13d.Articoli;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 19.22
 */


/**
 *
 *  Classe che raccoglie tutti i metodi statici (globali) da usare nel progetto
 *
 *
 */
public class Global {
    /**
     *
     * @return il seguente id disponibile all'aggiunta di un nuovo prodotto
     */
    public static int get_next_id (){
       general_counter++;
        return general_counter;
    }

    /**
     * Questo metodo setta il contatore generale degli id dei prodotti
     * 
     * @param general_counter - contatore id
     */
    public static void setGeneral_counter(int general_counter) {
        Global.general_counter = general_counter;
    }
    
    /**
     * 
     * @return il contatore generale degli id.
     */
    public static int getGeneral_counter() {
        return general_counter;
    }

    /**
     *  E' il contatore statico privato
     */
 private static int general_counter;


}
