package it.unisa.info13d.Articoli;

/**
 * Created with MONSTER.
 * User: xgiovio
 * Date: 16/12/13
 * Time: 18.53
 */
public interface Utilizzabile {
    /**
     *  Verifica se un metodo e' acquistabile
     *
     * @return se un bene e' acquistabile
     */
    boolean eAcquistabile();

}
